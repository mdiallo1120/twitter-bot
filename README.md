# Twitter-Bot

## Getting started

If you want to use the code to add new functionality,
 please go to the official documentation: https://docs.tweepy.org/en/stable/ 
For all imports,
 don't forget to set up the virtual environment for imports and 
 module downloads: https://packaging.python.org/en/latest/guides/installing-using-pip-and-virtual-environments/
Good luck and enjoy!


## Description

This project is a Twitter bot that automates certain tasks on Twitter. Like adding, sharing and many other features that you can find in the document I've linked to in the documentation.

## Installation

1. Clone this repository on your local machine.
2. Install the dependencies using the command `pip install -r requirements.txt`.

## Configuration

Create a `.env` file at the root of the project and add the following environment variables:

```plaintext
TWITTER_API_KEY=your_twitter_api_key
TWITTER_API_SECRET_KEY=your_twitter_api_secret_key
TWITTER_ACCESS_TOKEN=your_twitter_access_token
TWITTER_ACCESS_TOKEN_SECRET=your_twitter_access_token_secret
